package com.example.draw;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {


    private Spinner spinner;

    private DrawTool drawTool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        this.drawTool = (DrawTool) findViewById(R.id.lienzo);
        System.out.println("DrawTool"+ this.drawTool.toString());

        this.spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.spinner_options,
                android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        this.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int
                    i, long l) {
                String selected = adapterView.getSelectedItem().toString();
                System.out.println("Selected item: " + selected.toString());
                drawTool.setSelectedColor(selected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }
}