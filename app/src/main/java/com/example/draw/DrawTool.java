package com.example.draw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Spinner;

import androidx.annotation.Nullable;


import java.util.ArrayList;
import java.util.List;

public class DrawTool extends View {

    private float xPos, yPos = 0;

    private Path path;
    private Paint paint;
    private List<Path> pathsList;
    private List<Paint> paintList;
    private String selectedColor;


    public DrawTool(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.pathsList = new ArrayList<>();
        this.paintList = new ArrayList<>();
        this.paint = new Paint();
    }

    public void setSelectedColor(String selectedColor) {
        this.selectedColor = selectedColor;
    }

   public void setPaintColor(Paint paintObj, String paint) {

        switch(paint.toUpperCase()) {
            case "RED":
                System.out.println("Current color: RED");
                paintObj.setColor(Color.RED);
                break;
            case "BLUE":
                System.out.println("Current color: BLUE");
                paintObj.setColor(Color.BLUE);
                break;
            case "YELLOW":
                System.out.println("YELLOW");
                paintObj.setColor(Color.YELLOW);
                break;
            case "GREEN":
                System.out.println("GREEN");
                paintObj.setColor(Color.GREEN);
                break;
            case "BLACK":
                System.out.println("Current color: BLACK");
                paintObj.setColor(Color.BLACK);
                break;
            case "WHITE":
                System.out.println("Current color: WHITE");
                paintObj.setColor(Color.WHITE);
                break;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int i = 0;
        for (Path path : pathsList) {
            canvas.drawPath(path, paintList.get(i));                  //Path + Paint
            i++;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        xPos = event.getX();
        yPos = event.getY();

        Spinner s = getRootView().findViewById(R.id.spinner);   //TODO: Revise this, might be eating RAM

        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Paint newPaint = new Paint();                        //Create new Paint object

                newPaint.setStrokeWidth(20);                   //Paint config
                newPaint.setStyle(Paint.Style.STROKE);
                setPaintColor(newPaint, this.selectedColor);
                paintList.add(newPaint);                       //Add paint to list

                //Trazado
                path = new Path();                          //Create new Path

                path.moveTo(xPos, yPos);                    //Path points
                pathsList.add(path);                        //Add path

                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:

                int puntosHistoricos = event.getHistorySize();
                for(int i = 0; i < puntosHistoricos; i++) {
                    path.lineTo(event.getHistoricalX(i), event.getHistoricalY(i));
                }

        }
        invalidate(); //Actualizar pantalla
        return true;
    }

    @Override
    public String toString() {
        return "DrawTool{" +
                "xPos=" + xPos +
                ", yPos=" + yPos +
                ", path=" + path +
                ", paint=" + paint +
                ", pathsList=" + pathsList +
                ", paintList=" + paintList +
                '}';
    }
}
